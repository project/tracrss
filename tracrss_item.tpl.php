<?php


/**
 * This renders a single ticket.  Variables available are:
 *
 * $item_title    Ticket title
 * $item_link     Link to the ticket in the trac as plain text
 * $item_owner    The owner of the ticket
 * $display_link  A link condtructed as <a href="$item_link">$item_title</a>
 *
 * And the raw DOM node is available as:
 * $xml_node
 *
 * This will dump the node:
 * foreach ( $xml_node->childNodes as $node )
 * {
 * 	   echo "<div>" . $node->nodeName . " = " . $node->nodeValue . "</div>";
 * }
 */
?>

<?php echo $display_link; 

