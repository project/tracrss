<?php


/**
 The default function.  It gets all the tickets from the servers and displays it.
 */
function tracrss_tickets() {
  // Set page title
  drupal_set_title(t("Trac RSS"));

  // Get the logged in user and load the user specific config
  global $user;
  $user_config = tracrss_userconfig_get($user->uid);

  // the item array will hold <li> objects, each of which is one trac server
  $items = array();

  // get the list of trac severs and loop through them
  $servers = variable_get('tracrss_servers', array());
  foreach ($servers as $id => $server) {
    // get the user list, note that the current user's login name is always included as the first array item
    $userlist = tracrrs_get_userlist_from_config($user_config, $id, $user->name);

    // get the trac url as a web page
    $url = tracrss_make_url($server['url'], $userlist, FALSE);

    // build the title
    $title = t
    (
      '<a href="@editlink">[edit]</a> Tickets for %userlist from trac server %tracname  (<a href="@serverlink">%serverlink</a>)',
      array
      (
        '@editlink' => url('tracrss/edit/$id'),
        '%userlist' => implode(",", $userlist),
        '%tracname' => $server['name'],
        '@serverlink' => url($server['url']),
        '%serverlink' => $server['url'],
      )
    );

    // is this server enabled?
    if ($user_config[$id]['enabled'] != "disabled") {
      // fetch the rss items for this user(s) on this server
      $rss_items = tracrss_fetch_tickets($server, $userlist, $user->uid);
      // $_items will be a list of tickets
      $_items = array();

      foreach ($rss_items as $_item) {
        $_items[] = theme("tracrss_item", $_item);
      }

      // send the ticket list for theming
      $items[] = $title . theme('tracrss_list', $_items);
    }
    // if the server is diabled list it empty but this includes a link to edit it's settings else how would it ever be re-enabled
    else {
      $items[] = $title;
    }
  }

  // send the whole top level list for theming
  $output = theme('tracrss_list', $items);
  return $output;
}

/**
 The per user config of a given server.  For each server a user may enable/disable it, and may specifiy additional user names to retrieve tickets for it.  e.g. you could check the tickets for a team.
 */
function tracrss_config($id) {
  // get logged in user
  global $user;
  // get the server list and then this user
  $servers = variable_get('tracrss_servers', array());
  $server = $servers[$id];

  // set the page title
  drupal_set_title(t("Trac settings for %name on trac server %trac", array("%name" => $user->name, "%trac" => $server['name'])));

  // get the config form
  return drupal_get_form('tracrss_config_form', $id);
}

/**
 The configuration form for a user on a given server.  Each server can be enabled/disabled and can have additional user name associated with it for retrieving tickets.
 */
function tracrss_config_form(&$form_state, $id) {
  // get the logged in user and all the server configs for that user if they have any.
  global $user;
  $uid = $user->uid;
  $config = tracrss_userconfig_get($uid);

  // get the config of the server we are editing
  $server = $config[$id];

  // build the form
  // add enabled check box
  $form['tracrss_config_server_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Retrive tickets from this server'),
    '#default_value' => $server['enabled'] != "disabled",
    '#description' => t('The name used to identify this trac. Any name will do it is only used by humans adminstrating this module.'),
  );
  // add username list
  $userlist = is_array($server['userlist']) ? implode(",", $server['userlist']) : "";
  $form['tracrss_config_username_list'] = array(
    '#type' => 'textfield',
    '#title' => t('Userlist'),
    '#default_value' => $userlist,
    '#maxlength' => 128,
    '#size' => 45,
    '#description' => t('By default only tickets for the current (drupal) logged in user is checked for.  To specify additional ticket owners list them here, comma seperated.'),
  );
  // add server id
  $form['tracrss_config_server_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $id,
  );
  // add submit button
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save settings'));

  return $form;
}

/**
 process the form submit
 */
function tracrss_config_form_submit(&$form_state, $form) {
  // if we have permission to do this, build the array for this user on this server and save it
  if (user_access('config own tracrss')) {
    // check if it's enabled
    $enabled = ($form['values']['tracrss_config_server_enabled']) ? "enabled" : "disabled";
    // get the user list as an array
    $userlist = explode(",", $form['values']['tracrss_config_username_list']);
    // get this server id
    $serverid = $form['values']['tracrss_config_server_id'];

    // make the config array
    $server_config = array(
      'enabled' => $enabled,
      'userlist' => $userlist,
    );

    // get the logged in user
    global $user;
    $uid = $user->uid;
    // get the loggen in users config (for all servers)
    $config = tracrss_userconfig_get($uid);
    // replace (or add) this server config and save it.
    $config[$serverid] = $server_config;
    tracrss_userconfig_set($uid, $config);
  }
  $form_state['#redirect'] = 'tracrss';
}

