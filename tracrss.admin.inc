<?php


/**
 * The default admin pages list of trac servers.  It creates the array of headers and data for all the alailable servers and provides mechanisms for editing or deleting them.
 */
function tracrss_admin_list_servers($form_state = NULL, $lang = NULL) {
  // Get the existing servers
  $servers = variable_get('tracrss_servers', array());

  // Create array of headers
  $header = array(
    array('data' => t('Edit')),
    array('data' => t('Trac Name'), 'field' => 'name', 'sort' => 'asc'),
    array('data' => t('Trac URL'), 'field' => 'url'),
    array('data' => t('Username'), 'field' => 'username'),
    array('data' => t('Password'), 'field' => 'password'),
    array('data' => t('Delete')),
  );

  // create an array for the data
  $rows = array();
  // loop through the servers
  foreach ($servers as $id => $server) {
    // create the data for each cell in the table
    $rows[] = array(
      // link to edit page
      l(t('edit'), "admin/settings/tracrss/edit/$id"),
      // internal server name
      check_plain($server['name']),
      // the URL of the trac server
      l($server['url'], $server['url']),
      // the username
      check_plain($server['username']),
      // and password to use to auth with on the trac
      check_plain($server['password']),
      // link to delete
      l(t('delete'), "admin/settings/tracrss/delete/$id"),
    );
  }

  // if there's no data show an informative message
  if (empty($rows)) {
    $empty_message = t('No trac servers specified.');
    $rows[] = array(array('data' => $empty_message, 'colspan' => 9));
  }

  // pass the headers and data to the theme subsystem and return the html
  $output .= theme('table', $header, $rows);
  return $output;
}

/** eddit/add a new trac server*/
function tracrss_admin_edit($id = NULL) {
  // if there's an id pass it to the form builder function
  if ($id) {
    $output = drupal_get_form('tracrss_admin_server_form', $id);
  }
  else {
    $output = drupal_get_form('tracrss_admin_server_form');
  }
  // return the html
  return $output;
}

/** add/edit server form */
function tracrss_admin_server_form(&$form_state, $id = NULL) {
  // get all servers and then the server we are editing.  due to the slack nature of php if $id is NULL, $servers[NULL] will be NULL
  $servers = variable_get('tracrss_servers', array());
  $server = $servers[$id];

  // build the form, adding the default data if available.  For more information see the formapi section of the drupal site
  // plain english server name
  $form['tracrss_admin_server_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Trac Name'),
    '#default_value' => $server['name'],
    '#maxlength' => 128,
    '#size' => 45,
    '#description' => t('The name used to identify this trac. Any name will do it is only used by humans adminstrating this module.'),
    '#required' => TRUE,
  );
  // server url
  $form['tracrss_admin_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#default_value' => $server['url'],
    '#maxlength' => 128,
    '#size' => 75,
    '#description' => t('The URL that is used to reach the front page of this trac.  The path "/query" will be appended to this to retrieve the ticket data.'),
    '#required' => TRUE,
  );
  // username to auth with
  $form['tracrss_admin_server_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => $server['username'],
    '#maxlength' => 128,
    '#size' => 25,
    '#description' => t('The username to access the trac.  This user will need the VIEW_TICKET permissions in the trac server.'),
    '#required' => TRUE,
  );
  // password to auth with
  $form['tracrss_admin_server_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => $server['password'],
    '#maxlength' => 128,
    '#size' => 25,
    '#description' => t('The password that matches the username specified above.'),
    '#required' => TRUE,
  );
  // if we are an edit form then include the id as a hidden field
  if ($id) {
    $form['tracrss_admin_server_id'] = array(
      '#type' => 'hidden',
      '#default_value' => $id,
    );
  }
  // switch the submit button dependent on if we are an edit/add form
  if ($id) {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Update server'));
  }
  else {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Add new server'));
  }
  return $form;
}

// _TODO: toby function tracrss_admin_server_form_validate ( &$form_state, $name = NULL )

/** process add/edit server form */
function tracrss_admin_server_form_submit(&$form_state, $form) {
  // get the id of the form submited
  $id = $form['values']['tracrss_admin_server_id'];

  // get the specified server details
  $server = array(
    "name" => $form['values']['tracrss_admin_server_name'],
    "url" => $form['values']['tracrss_admin_server_url'],
    "username" => $form['values']['tracrss_admin_server_username'],
    "password" => $form['values']['tracrss_admin_server_password'],
  );

  // get all the servers we know about
  $servers = variable_get('tracrss_servers', array());

  // if we have an id set the details for that server
  if ($id && !empty($servers[$id])) {
    $servers[$id] = $server;
    // save to the drupal variable store and redirect to the front page
    variable_set('tracrss_servers', $servers);
    drupal_set_message(t('The server has been saved.'));
    $form_state['#redirect'] = array('admin/settings/tracrss');
    return;
  }

  // we couldn't find a matching server, it's a new one
  if (count($servers) > 0) {
    // create an id greater that the biggest in the existing server array
    $new_id = 1 + max(array_keys($servers));
  }
  else {
    // create a new one as id 1
    $new_id = 1;
  }
  // set server dat
  $servers[$new_id] = $server;
  //
  variable_set('tracrss_servers', $servers);
  drupal_set_message(t('The server has been created.'));
  // save to the drupal variable store and redirect to the front page
  $form_state['#redirect'] = array('admin/settings/tracrss');
}

/** confirm delete a trac server */
function tracrss_admin_delete_server_confirm($form_state, $id) {
  // get all servers and then the server we are to delete
  $servers = variable_get('tracrss_servers', array());
  $server = $servers[$id];

  // check we have access to do this
  if (user_access('administer tracrss')) {
    // get the form id
    $form['trac_id'] = array('#type' => 'value', '#value' => $id);
    // create the confirm id
    $output = confirm_form(
      $form,
      t('Are you sure you want to delete server: %title?', array('%title' => $server['name'])),
      isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/tracrss'
    );
  }
  return $output;
}

/** delete the server */
function tracrss_admin_delete_server_confirm_submit($form, &$form_state) {
  // check the form was confirmed
  if ($form_state['values']['confirm']) {
    // get the id
    $id = $form_state['values']['trac_id'];
    // get the server list and unset the matching element
    $servers = variable_get('tracrss_servers', array());
    unset($servers[$id]);
    // save and redirect
    variable_set('tracrss_servers', $servers);
    $form_state['#redirect'] = array('admin/settings/tracrss');
  }
}

